﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Agendar.WebService.Models
{
    [Table("Agenda")]
    public class Agenda
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public virtual int Id { get; set; }
        [Required]
        public virtual string Title { get; set; }
        [Required]
        public virtual DateTime Date { get; set; }
        [Required]
        public virtual DateTime TimeStart { get; set; }
        public virtual DateTime TimeEnd { get; set; }
        public virtual string Description { get; set; }
        public virtual string Location { get; set; }
        public virtual DateTime? AddedOn { get; set; } = DateTime.Now;
        public virtual DateTime? DeletedOn { get; set; }
    }

    public class AgendaTimeSpan
    {
        public virtual DateTime Start { get; set; }
        public virtual DateTime End { get; set; }
    }
}
