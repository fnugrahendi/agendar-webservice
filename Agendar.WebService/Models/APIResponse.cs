﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Agendar.WebService.Models
{
    public class APIResponse
    {
        public int ReturnStatus { get; set; }
        public DateTime ResponseTime { get; set; }
        public string ResponseBody { get; set; }
        public string? ErrorMessage { get; set; }
    }
}
