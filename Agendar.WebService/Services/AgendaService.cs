﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Agendar.WebService.Interfaces;
using Agendar.WebService.Models;

namespace Agendar.WebService.Services
{
    public class AgendaService
    {
        private readonly IDataProvider<Agenda> _agendaProvider;
        public AgendaService(IDataProvider<Agenda> dataProvider)
        {
            _agendaProvider = dataProvider;
        }
        public IEnumerable<Agenda> GetAgendas()
        {
            return _agendaProvider.GetAll();
        }
        public IEnumerable<Agenda> GetAgendasByDate(DateTime date)
        {
            return _agendaProvider.GetAll().Where<Agenda>(ag => ag.Date == date);
        }
        public async Task<bool> IsDateTimeAvailableAsync(DateTime start, DateTime end, bool includePublicHoliday = false)
        {
            List<Agenda> overlappingAgendas = _agendaProvider.GetAll().Where(ag => CheckOverlap(start, end, ag.TimeStart, ag.TimeEnd)).ToList();
            bool isAvailable = overlappingAgendas.Count == 0;
            if (isAvailable && includePublicHoliday)
            {
                isAvailable = !await CheckIsPublicHoliday(start.Date, end.Date);
            }
            return isAvailable;
        }
        public Agenda GetAgendaById(int id)
        {
            return _agendaProvider.GetById(id);
        }
        public IEnumerable<Agenda> GetAgendasByTitle(string title)
        {
            return _agendaProvider.GetAll().Where(ag => ag.Title == title);
        }
        public async Task<Agenda> InsertAgenda(Agenda agenda)
        {
            return await _agendaProvider.Insert(agenda);
        }
        public bool UpdateAgenda(Agenda agenda)
        {
            try
            {
                Agenda selectedAgenda = _agendaProvider.GetAll().First(ag => ag.Id == agenda.Id);
                
                if (selectedAgenda != null) _agendaProvider.Update(selectedAgenda);
                else return false;

                return true;
            }
            catch
            {
                return false;
            }
        }
        public bool DeleteAgenda(int id)
        {
            try
            {
                Agenda selectedAgenda = _agendaProvider.GetAll().First(ag => ag.Id == id);

                if (selectedAgenda != null) _agendaProvider.Delete(selectedAgenda);
                else return false;

                return true;
            }
            catch
            {
                return false;
            }
        }
        private static bool CheckOverlap(DateTime startA, DateTime endA, DateTime startB, DateTime endB)
        {
            if (
                (startA < startB && startB < endA) || 
                (startA < endB && endB <= endA) || 
                (startB < startA && endB > endA)
                ) return true;
            else return false;
        }
        private static async Task<bool> CheckIsPublicHoliday(DateTime startDate, DateTime endDate)
        {
            List<PublicHoliday> publicHolidays = await PublicHolidayService.GetPublicHoliday("SG");
            bool isPublicHoliday = publicHolidays.FindIndex(ph => ph.Date == startDate.Date || ph.Date == endDate.Date) > -1;
            return isPublicHoliday;
        }
    }

}
