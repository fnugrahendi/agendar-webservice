﻿using System;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace Agendar.WebService.Services
{
    public class PublicHolidayService
    {
        public static async Task<List<PublicHoliday>> GetPublicHoliday(string countryCode)
        {
            try
            {
                string API_URL =String.Format("https://date.nager.at/api/v3/PublicHolidays/2022/{0}", countryCode);
                var client = new HttpClient();
                var response = await client.GetAsync(API_URL);
                var result = await response.Content.ReadAsStringAsync();
                
                var publicHoliday = JsonConvert.DeserializeObject<List<PublicHoliday>>(result);

                return publicHoliday;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return null;
            }
        }
    }

    public class PublicHoliday
    {
        public string Name { get; set; }
        public string CountryCode { get; set; }
        public DateTime Date { get; set; }

    }
}
