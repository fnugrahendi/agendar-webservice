﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using Agendar.WebService.Models;


namespace Agendar.WebService.Infrastructures
{
    public class AgendarContext : DbContext
    {
        public AgendarContext(DbContextOptions<AgendarContext> options) : base(options)
        {
        }
        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
        }
        public DbSet<Agenda> Agenda { get; set; }
    }
}
