﻿using Agendar.WebService.Interfaces;
using Agendar.WebService.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Agendar.WebService.Infrastructures
{
    public class AgendaDataProvider: IDataProvider<Agenda>
    {
        AgendarContext _dataContext;
        public AgendaDataProvider(AgendarContext context)
        {
            _dataContext = context;
        }
        public void Delete(Agenda agenda)
        {
            try
            {
                _dataContext.Agenda.Remove(agenda);
                _dataContext.SaveChanges();
            }
            catch
            {
                throw;
            }
        }

        public IEnumerable<Agenda> GetAll()
        {
            try
            {
                List<Agenda> agendas = _dataContext.Agenda.Where(ag => ag.DeletedOn == null).ToList();
                return agendas;
            } 
            catch
            {
                throw;
            }
            
        }

        public Agenda GetById(int Id)
        {
            try
            {
                Agenda agendas = _dataContext.Agenda.FirstOrDefault(ag => ag.Id == Id && ag.DeletedOn == null);
                return agendas;
            }
            catch
            {
                throw;
            }
        }

        public async Task<Agenda> Insert(Agenda newObject)
        {
            try
            {
                var obj = await _dataContext.Agenda.AddAsync(newObject);
                await _dataContext.SaveChangesAsync();
                return obj.Entity;
            }
            catch
            {
                throw;
            }
        }

        public void Update(Agenda updatedObject)
        {
            try
            {
                _dataContext.Agenda.Update(updatedObject);
                _dataContext.SaveChanges();
            }
            catch
            {
                throw;
            }
        }
    }
}
