﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Agendar.WebService.Interfaces
{
    public interface IDataProvider<T>
    {
        IEnumerable<T> GetAll();
        T GetById(int Id);
        void Update(T updatedObject);
        Task<T> Insert(T newObject);
        void Delete(T deletedObject);

    }
}
