﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Agendar.WebService.Models;
using Agendar.WebService.Services;
using Newtonsoft.Json;
 

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Agendar.WebService.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AgendaController : ControllerBase
    {
        private AgendaService _agendaService;
        public AgendaController(AgendaService service)
        {
            _agendaService = service;
        }

        [HttpGet]
        public APIResponse Get()
        {
            IEnumerable<Agenda> agendas = _agendaService.GetAgendas();
            APIResponse response = new()
            {
                ResponseBody = JsonConvert.SerializeObject(agendas),
                ReturnStatus = 1,
                ResponseTime = DateTime.Now
            };
            return response;
        }

        [HttpGet("{id}")]
        public APIResponse GetAgendaById(int id)
        {
            Agenda agendas = _agendaService.GetAgendaById(id);
            APIResponse response = new()
            {
                ResponseBody = JsonConvert.SerializeObject(agendas),
                ReturnStatus = 1,
                ResponseTime = DateTime.Now
            };
            return response;
        }

        [HttpGet("/date/{date}")]
        public APIResponse GetAgendaByDate(DateTime date)
        {
            IEnumerable<Agenda> agendas = _agendaService.GetAgendasByDate(date);
            APIResponse response = new()
            {
                ResponseBody = JsonConvert.SerializeObject(agendas),
                ReturnStatus = 1,
                ResponseTime = DateTime.Now
            };
            return response;
        }

        [HttpGet("checkIsAvailable")]
        public async Task<APIResponse> CheckAvailability([FromBody] AgendaTimeSpan timeSpan)
        {
            bool isDateTimeAvailable = await _agendaService.IsDateTimeAvailableAsync(timeSpan.Start, timeSpan.End, true);
            APIResponse response = new()
            {
                ResponseBody = isDateTimeAvailable ? "Available" : "Not Available",
                ReturnStatus = 1,
                ResponseTime = DateTime.Now
            };
            return response;
        }
        [HttpPost]
        public async Task<APIResponse> Post([FromBody] Agenda agenda)
        {
            Agenda newAgenda = new();
            string errorMsg = "";
            bool isDateTimeAvailable = await _agendaService.IsDateTimeAvailableAsync(agenda.TimeStart, agenda.TimeEnd);
            if (!isDateTimeAvailable)
            {
                errorMsg = "DATETIME_NOT_AVAILABLE";
            }
            else
            {
                try
                {
                    newAgenda = await _agendaService.InsertAgenda(agenda);
                }
                catch (Exception ex)
                {
                    errorMsg = ex.Message;
                }
            }
            return new APIResponse()
            {
                ResponseBody = !string.IsNullOrEmpty(errorMsg) ? null : JsonConvert.SerializeObject(newAgenda),
                ReturnStatus = 1,
                ResponseTime = DateTime.Now,
                ErrorMessage = errorMsg
            };
        }

        [HttpPut]
        public async Task<APIResponse> Put([FromBody] Agenda agenda)
        {
            bool isUpdated = false;
            bool isDateTimeAvailable = await _agendaService.IsDateTimeAvailableAsync(agenda.TimeStart, agenda.TimeEnd);
            string errorMsg = "";
            if (!isDateTimeAvailable)
            {
                errorMsg = "DATETIME_NOT_AVAILABLE";
            }
            else
            {
                try
                {
                    isUpdated = _agendaService.UpdateAgenda(agenda);
                }
                catch (Exception ex)
                {
                    errorMsg = ex.Message;
                }
            }            
            return new APIResponse()
            {
                ResponseBody = isUpdated ? "UPDATE_SUCCESS" : "UPDATE_FAILED",
                ReturnStatus = 1,
                ResponseTime = DateTime.Now,
                ErrorMessage = errorMsg
            };
        }

        [HttpDelete("{id}")]
        public APIResponse Delete(int id)
        {
            bool isDeleted = false;
            string errorMsg = "";
            try
            {
                isDeleted = _agendaService.DeleteAgenda(id);
            }
            catch (Exception ex)
            {
                errorMsg = ex.Message;
            }
            return new APIResponse()
            {
                ResponseBody = isDeleted ? "DELETE_SUCCESS" : "DELETE_FAILED",
                ReturnStatus = 1,
                ResponseTime = DateTime.Now,
                ErrorMessage = errorMsg
            };
        }

        [HttpGet("/checkPH")]
        public async Task<APIResponse> CheckPH()
        {
            var holiday = await PublicHolidayService.GetPublicHoliday("SG");
            APIResponse response = new()
            {
                ResponseBody = JsonConvert.SerializeObject(holiday),
                ReturnStatus = 1,
                ResponseTime = DateTime.Now
            };
            return response;
        }
    }
}
